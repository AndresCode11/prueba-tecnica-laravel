<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('health', function() {
    return response()->json(['status' => 'ok'])
        ->setStatusCode(200);
});

Route::group(['prefix' => 'item'], function(){
    Route::get('showall', 'user\UserDataController@showAll');
    Route::post('create', 'user\UserDataController@createItem');
    Route::post('edit', 'user\UserDataController@editItem');
    Route::post('delete', 'user\UserDataController@deleteItem');
});
