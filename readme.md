<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Despliegue Aplicativo

1. En caso de no tener el gestor de dependencias composer instalar(tener previamente instalado php 7.2)

2. Clonar el repositorio:
   Usando el comando git clone https://gitlab.com/AndresCode11/prueba-tecnica-laravel.

2. Instalar depedencias:
   Usando el comando composer install

3. Instalar llaves proyecto laravel:
   Usando el comando  php artisan key:generate

4. Agreagar archivo .env al proyecto(solicitar al desarollador(archivo .env no se agrego al repositorio por cuestiones de seguridad con el servidor))
   En el archivo .env se ecuentran las variables de entorno del archivo y credecniales del servidor y base de datos.

    Actualmente se encuentra el servidor de produccion funcionando en http://159.203.184.170:8000
    
    En caso de querer realizar la migracion con credecniales diferentes de gestor de base de datos realizar el cambio de credenciales en el .env

5. ejecutar las migraciones:
   Usando el comando php artisan migrate

6. Para realizar prueba en local host:
   Usando el comando php artisan serve

7. Para realizar un depliegue a produccion agregar el proyecto con la configuracion necesaria segun el servidor web que se use
8. para realizar un despliegue que salga a internet mediante la ip publica sin necesidad de instalar servidor web usar el comando:
  php artisan serve --host="mi ip publica"


      

    
