<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\user\userData\UserDataManagement;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserDataController extends Controller
{
    private $management;

    public function __construct()
    {
        $this->management = new UserDataManagement();
    }

    // Metodo para visualizar todos los datos del endPoint
    public function showAll()
    {
        try 
        {
            return response()
                ->json($this->management->getAllItems())
                ->setStatusCode(JsonResponse::HTTP_OK);

        } catch (\Exception $exception) {
            
            return response()
                ->json(['error' => $exception->getMessage()])
                ->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Metodo para crear Item
    public function createItem(Request $request)
    {
        $data = $request->all();
        $this->management->setData($data);
        try 
        {
            return response()
                ->json($this->management->createItem())
                ->setStatusCode(JsonResponse::HTTP_OK);

        } catch (\Exception $exception) {
            
            return response()
                ->json(['error' => $exception->getMessage()])
                ->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Metodo para eliminar Item
    public function deleteItem(Request $request)
    {
        try 
        {
            return response()
                ->json($this->management->deleteItem($request->get('itemId')))
                ->setStatusCode(JsonResponse::HTTP_OK);

        } catch (\Exception $exception) {
            
            return response()
                ->json(['error' => $exception->getMessage()])
                ->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Metodo para editar Item
    public function editItem(Request $request)
    {
        $userId = $request->get('itemId');
        $tittle = $request->get('title');
        try 
        {
            return response()
                ->json($this->management->editItem($userId, $tittle))
                ->setStatusCode(JsonResponse::HTTP_OK);

        } catch (\Exception $exception) {
            
            return response()
                ->json(['error' => $exception->getMessage()])
                ->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
