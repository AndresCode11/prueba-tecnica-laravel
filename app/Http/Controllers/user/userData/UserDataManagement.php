<?php namespace App\Http\Controllers\user\userData;

use App\Models\UserData;

//clase para gestionar la logica entre las rutas y los modelos
class UserDataManagement
{
    private $data;

    public function getAllItems() 
    { 
        return UserData::getAll()->get()
            ->toArray();
    }

    public function createItem() 
    {
        UserData::createItem([
            'tittle' => $this->data['tittle']
        ]);

        return 'Item creado';
    }

    public function deleteItem($itemId)
    {
        UserData::find($itemId)->delete();
        return 'Item elminado';
    }

    public function editItem($itemId, $tittle) 
    {
        $user = UserData::find($itemId);
        $user->tittle = $tittle;
        $user->save();

        return 'Item editado';

    }

    public function setData($data) {
        $this->data = $data;
    }
}