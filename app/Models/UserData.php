<?php namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'userdata';

    protected $fillable = [
        'tittle'
    ];

    public static function getAll(): Builder
    {
        return UserData::select(
            'id',
            'tittle'
        );
    }

    public static function createItem($data)
    {
        UserData::create($data);
    }

    public static function edit($itemId)
    {

    }
}
